# CMake generated Testfile for 
# Source directory: C:/opencv/opencv_source/tests
# Build directory: C:/opencv/opencv_bin_win32/tests
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(cv)
SUBDIRS(cxcore)
SUBDIRS(ml)
SUBDIRS(cxts)
