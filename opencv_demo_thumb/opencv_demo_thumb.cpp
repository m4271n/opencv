// opencv_demo_thumb.cpp : Defines the entry point for the console application.
//

#include "cv.h" 
#include "cxcore.h" 
#include "highgui.h"
#include <cstdio>

int main(int argc, char** argv[])
{
	//defines the name of the input picture
	char * lettername = "inputpic_stoer_.jpg";

	system("abc.exe");

	//Load the color image into an array
	//src points to the memory location where the input image is saved
	printf("Load Image \r\n");
	IplImage *src = cvLoadImage( lettername, CV_LOAD_IMAGE_COLOR );

	//check if loading was successfull
	if(NULL==src)
	{
		printf("Error: Couldn't open the file");
	}
	else
	{
		//convert input image to greyscale:
		//cvCreateImage allocates memory for the greyscale picture
		//the hough transformation works with greyscale images only
		printf("Convert to greyscale\r\n");
		IplImage *gsrc = cvCreateImage( cvGetSize(src), IPL_DEPTH_8U, 1 );
		cvCvtColor(src,gsrc,CV_RGB2GRAY);

		printf("Save converted input picture\r\n");
		cvSaveImage( "converted_input.jpg", gsrc );

		//The hough transformation returns several circles
		//so you have to create store them somewhere
		//For this OpenCV implemented CvMemStorage
		CvMemStorage* storage = cvCreateMemStorage(0);
		//The hough transformation works better with a slightly blurred image
		cvSmooth(gsrc,gsrc, CV_GAUSSIAN, 5,5);
		//This function detects the circles, saves them in the memory storage 
		//an returns the pointer to the first entry
		CvSeq* results = cvHoughCircles(gsrc,storage,CV_HOUGH_GRADIENT,2,src->width/10,100,300);

		//draw the found circles on the source image
		for(int i = 0;i<results->total;i++)
		{
			//get the first value from the memory storage
			float* p = (float*) cvGetSeqElem(results,i);
			CvPoint pt = cvPoint(cvRound(p[0]),cvRound(p[1]));
			//draw a red circle
			cvCircle(src,pt,cvRound(p[2]),CV_RGB(0xff,0x00,0x00),3);
		}

		//check if the container contains something with red, green or blue color
		for(int i = 0;i<results->total;i++)
		{
			//again, get the values from the memory storage
			float* p = (float*) cvGetSeqElem(results,i);
			CvPoint pt = cvPoint(cvRound(p[0]),cvRound(p[1]));
			//get the color of the pixel of the center of the detected circle
			CvScalar color= cvGet2D(src,pt.y,pt.x);
			//if blue (ChannelSequence in src-image is switched: BGR not RGB)
			if(color.val[0]>150)
				cvCircle(src,pt,10,CV_RGB(0x00,0x00,0xff),-1);
			//if green
			if(color.val[1]>150)
				cvCircle(src,pt,8,CV_RGB(0x00,0xff,0x00),-1);
			//if red
			if(color.val[2]>150)
				cvCircle(src,pt,6,CV_RGB(0xff,0x00,0x00),-1);
		}

		printf("Save changed source file\r\n");
		cvSaveImage( "detected.jpg", src );

		//Window functions does not work on windows CE
#ifndef WINCE
		//*Show the pic*/
		printf("Show pics \r\n");
		cvNamedWindow("Source", CV_WINDOW_AUTOSIZE);
		cvShowImage("Source", src);
		
		//cvNamedWindow("Converted Source", CV_WINDOW_AUTOSIZE);
		//cvShowImage("Converted Source", gsrc);

		//wait for user to press a button
		cvWaitKey(0);
		cvDestroyWindow("Source");
		//cvDestroyWindow("Converted Source");
#endif
		//release the memory that was used for the pictures
		cvReleaseImage(&src);
		cvReleaseImage(&gsrc);
	}

	return 0;
}

