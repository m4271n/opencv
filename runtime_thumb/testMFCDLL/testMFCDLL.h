// testMFCDLL.h : main header file for the testMFCDLL DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"

// CtestMFCDLLApp
// See testMFCDLL.cpp for the implementation of this class
//

class CtestMFCDLLApp : public CWinApp
{
public:
	CtestMFCDLLApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

