//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft shared
// source or premium shared source license agreement under which you licensed
// this source code. If you did not accept the terms of the license agreement,
// you are not authorized to use this source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the SOURCE.RTF on your install media or the root of your tools installation.
// THE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES.
//
////////////////////////////////////////////////////////////////////////////////
//
//  TUXTEST TUX DLL
//
//  Module: main.h
//          Header for all files in the project.
//
//  Revision History:
//
////////////////////////////////////////////////////////////////////////////////

#ifndef __MAIN_TUX_H__
#define __MAIN_TUX_H__

////////////////////////////////////////////////////////////////////////////////
// Included files

#include <windows.h>
#include <stdlib.h>
#include <tchar.h>
#include <tux.h>
// #include <kato.h>

////////////////////////////////////////////////////////////////////////////////
// Suggested log verbosities

#define LOG_EXCEPTION          0
#define LOG_FAIL               2
#define LOG_ABORT              4
#define LOG_SKIP               6
#define LOG_NOT_IMPLEMENTED    8
#define LOG_PASS              10
#define LOG_DETAIL            12
#define LOG_COMMENT           14

// extern CKato            *g_pKato ;


////////////////////////////////////////////////////////////////////////////////
// kato logging macros

#define FAIL(x)     RETAILMSG( LOG_FAIL, (\
                        TEXT("FAIL in %s at line %d: %s"), \
                        TEXT(__FILE__), __LINE__, x ))

// same as FAIL, but also logs GetLastError value
#define ERRFAIL(x)  RETAILMSG( LOG_FAIL, (\
                        TEXT("FAIL in %s at line %d: %s; error %u"), \
                        TEXT(__FILE__), __LINE__, x, GetLastError() ))

#define ABORT(x)     RETAILMSG( LOG_ABORT, (\
                        TEXT("ABORT in %s at line %d: %s"), \
                        TEXT(__FILE__), __LINE__, x ))

// same as ABORT, but also logs GetLastError value
#define ERRABORT(x)  RETAILMSG( LOG_ABORT, (\
                        TEXT("ABORT in %s at line %d: %s; error %u"), \
                        TEXT(__FILE__), __LINE__, TEXT(#x), GetLastError() ))

#define WARN(x)     RETAILMSG( LOG_DETAIL, (\
                        TEXT("WARNING in %s at line %d: %s"), \
                        TEXT(__FILE__), __LINE__, x ))

// same as WARN, but also logs GetLastError value
#define ERRWARN(x)  RETAILMSG( LOG_DETAIL, (\
                        TEXT("WARNING in %s at line %d: %s; error %u"), \
                        TEXT(__FILE__), __LINE__, x, GetLastError() ))

// log's an error, but doesn't log a failure
#define ERR(x)      RETAILMSG( LOG_DETAIL, (\
                        TEXT("ERROR in %s at line %d: %s"), \
                        TEXT(__FILE__), __LINE__, TEXT(#x) ))

#define DETAIL(x)   RETAILMSG( LOG_DETAIL, ( x ))

#define PASS(x)     RETAILMSG( LOG_PASS, (TEXT(x) ))

#define COMMENT(x)  RETAILMSG( LOG_COMMENT, (TEXT(x) ))

#define SKIP(x)     RETAILMSG( LOG_SKIP, (x ))

#define UNIMPL(x)   RETAILMSG( LOG_NOT_IMPLEMENTED, (TEXT(x) ))

TESTPROCAPI GetTestResult(void);

void Log(LPWSTR szFormat, ...) ;


#endif // __MAIN_TUX_H__
